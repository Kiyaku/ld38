﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drill : MonoBehaviour {
	[System.Serializable]
	public class Info {
		public float Efficiency = 1;
		public float Speed = 1; // delay in seconds
		public float BonusEfficiency = 1;
	}

	public Info DrillInfo;
	public LineRenderer Beam;
	public AudioClip SFXDrill;
	public AudioClip SFXCollectResource;
	public bool IsDrilling = false;

	private float[] bonusSpeeds = new float[] { 1.30f, 1.60f, 2f };


	private void OnEnable() {
		ResearchManager.OnResearchUpdated += ResearchUpdated;
	}


	private void OnDisable() {
		ResearchManager.OnResearchUpdated -= ResearchUpdated;
	}


	private void ResearchUpdated() {
		int currentTier = ResearchManager.ins.GetCurrentTier(ResearchManager.ResearchID.SPACESHIP_1);

		if (currentTier > 0 && ResearchManager.ins.HasResearched(ResearchManager.ResearchID.SPACESHIP_1, currentTier)) {
			DrillInfo.BonusEfficiency = bonusSpeeds[currentTier - 1];
		}
	}


	public void Mine(Transform mineable) {
		IsDrilling = true;
		StartCoroutine(DoMine(mineable));
		StartCoroutine(ShowBeam(mineable));
	}

	
	private IEnumerator DoMine(Transform mineable) {
		IMineable imineable = mineable.GetComponent<IMineable>();

		while (true) {
			yield return new WaitForSeconds(DrillInfo.Speed);

			if (Vector2.Distance(mineable.position, transform.position) > 4) {
				Stop();
			}

			bool successful = imineable.Mine(DrillInfo);

			if (successful) {
				GameManager.ins.SFXPlayer.PlayOneShot(SFXCollectResource, 0.5f);
			}
		}
	}


	private IEnumerator ShowBeam(Transform mineable) {
		int beamCount = 10;
		float offset = 0.04f;
		Beam.positionCount = beamCount;
		Vector3[] points = new Vector3[beamCount];

		GameManager.ins.SFXPlayer.clip = SFXDrill;
		GameManager.ins.SFXPlayer.loop = true;
		GameManager.ins.SFXPlayer.Play();

		while (mineable != null) {
			Vector3 dir = mineable.position - transform.position;
			float length = dir.magnitude;

			for (int i = 0; i < beamCount; i++) {
				Vector3 pos = transform.position + ((dir / beamCount) * i);

				if (i != 0) {
					pos += new Vector3(Random.Range(-offset, offset), Random.Range(-offset, offset), 0);
				}

                points[i] = pos;
			}

			Beam.SetPositions(points);

			yield return null;
		}

		Stop();
    }


	public void Stop() {
		GameManager.ins.SFXPlayer.Stop();
		GameManager.ins.SFXPlayer.clip = null;
		GameManager.ins.SFXPlayer.loop = false;

		IsDrilling = false;

		Beam.positionCount = 0;

		StopAllCoroutines();
	}
}
