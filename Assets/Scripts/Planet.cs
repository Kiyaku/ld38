﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CircleCollider2D))]
public class Planet : MonoBehaviour {
	public Cloud CloudPrefab;
	public int MinClouds = 0;
	public int MaxClouds = 0;
	protected GameObject container;

	public int MaxHealth = 1;
	public int CurrentHealth {
		get;
		protected set;
	}


	protected virtual void Awake() {
		container = new GameObject(transform.name + " Container");
		container.transform.parent = transform;

		CurrentHealth = MaxHealth;

		if (CloudPrefab != null) {
			int count = Random.Range(MinClouds, MaxClouds);

			for (int i = 0; i < count; i++) {
				Cloud cloud = Instantiate(CloudPrefab, transform.position + new Vector3(0, 1, 0), Quaternion.identity);
				cloud.transform.RotateAround(transform.position, Vector3.back, Random.Range(0, 360));
                cloud.Generate(this);
			}
		}
    }


	public void AddToContainer(Transform t) {
		t.parent = container.transform;
	}


	public void Grow(float val, bool doAnimate = true) {
		container.transform.parent = null;

		if (doAnimate) {
			StartCoroutine(DoGrow(val));
		} else {
			Vector3 oldScale = transform.localScale;
			Vector3 newScale = oldScale + new Vector3(val, val, 0);
			transform.localScale = newScale;
		}
	}


	public void SetSize(Vector3 size) {
		transform.localScale = size;
	}


	private IEnumerator DoGrow(float val) {
		float timer = 0;
		float shakeOffset = 0.02f;
		Vector3 originalPos = transform.position;

		while (timer < 2) {
			timer += Time.deltaTime * 4f;

			transform.position = originalPos + new Vector3(Random.Range(-shakeOffset, shakeOffset), Random.Range(-shakeOffset, shakeOffset), 0);
			AdjustChildrens();
			yield return null;
		}

		transform.position = originalPos;
		timer = 0;
		Vector3 oldScale = transform.localScale;
		Vector3 newScale = oldScale + new Vector3(val, val, 0);

		while (timer < 1) {
			timer += Time.deltaTime * 2f;
			transform.localScale = Vector3.Slerp(oldScale, newScale, Mathf.SmoothStep(0, 1, timer));
			AdjustChildrens();
			yield return null;
        }

		transform.localScale = newScale;
		AdjustChildrens();
		container.transform.parent = transform;

		yield return null;
	}


	private void AdjustChildrens() {
		foreach (Transform child in container.transform) {
			if (child.GetComponent<IAdjustable>() != null) {
				child.GetComponent<IAdjustable>().Adjust();
            }
		}
	}

	
	public float Radius() {
		return transform.GetComponent<CircleCollider2D>().bounds.extents.x;
	}


	public void Shake(Vector3 dir, float strength) {
		StartCoroutine(DoShake(dir, strength));
    }


	private IEnumerator DoShake(Vector3 dir, float strength) {
		float timer = 0;
		float sin = 0;

		Vector3 originalPos = transform.position;

		while (timer < 1) {
			timer += Time.deltaTime * 5f;
			sin = timer * 180;

			transform.position = originalPos + (dir.normalized * strength) * Mathf.Sin(sin * Mathf.Deg2Rad);

			yield return null;
		}

		transform.position = originalPos;
	}
}
