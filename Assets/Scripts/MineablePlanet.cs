﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MineablePlanet : Planet, IMineable, IDestructable {
	public Resource.ResourceTypes ResourceType;
	public Transform DebrisPrefab;
	public int MaxResourceAmount;
	public int ResourcesPerDay = 1;
	public AudioClip SFXExplosion;
	public Sprite DeadPlanetSprite;
	private int resourceAmount;
	private Vector3 originalScale;

	public bool IsDepleted {
		get {
			return resourceAmount <= 0;
		}
	}


	override protected void Awake() {
		base.Awake();
		float scale = Random.Range(1.2f, 1.6f);
		MaxResourceAmount = Mathf.CeilToInt((float)MaxResourceAmount * scale);
        resourceAmount = MaxResourceAmount;
		transform.localScale = transform.localScale * Random.Range(1.2f, 1.6f);
        originalScale = transform.localScale;
    }


	private void OnEnable() {
		GameManager.OnDayChanged += Regenerate;
	}


	private void OnDisable() {
		GameManager.OnDayChanged -= Regenerate;
	}


	public void DealDamage(int amount) {
		if (CurrentHealth > 0) {
			CurrentHealth -= amount;

			if (CurrentHealth <= 0) {
				GameManager.ins.SFXPlayer.PlayOneShot(SFXExplosion, 0.5f);
                CurrentHealth = 0;
				DestroyObject();
			}
		}
	}


	public void DestroyObject() {
		float maxVelocity = 0.2f;
		int amount = Random.Range(3, 5);

		for (int i = 0; i < amount; i++) {
			Transform debris = Instantiate(DebrisPrefab);
			debris.position = transform.position;
			debris.eulerAngles = new Vector3(0, 0, Random.Range(0, 360));
			debris.GetComponent<Rigidbody2D>().velocity = new Vector2(Random.Range(-maxVelocity, maxVelocity), Random.Range(-maxVelocity, maxVelocity));
			debris.GetComponent<Rigidbody2D>().angularVelocity = Random.Range(-30, 30);
		}

		Destroy(gameObject);
    }


	private void UpdateSize() {
		float percentage = (float)resourceAmount / (float)MaxResourceAmount;
		SetSize(Vector3.Lerp(originalScale * 0.5f, originalScale, percentage));
	}


	public bool Mine(Drill.Info drill) {
		int amount = Mathf.FloorToInt(Mathf.CeilToInt((float)drill.Efficiency * drill.BonusEfficiency));

		if (resourceAmount < amount) {
			amount = resourceAmount;
		}

		if (resourceAmount == 0) {
			GetComponent<SpriteRenderer>().sprite = DeadPlanetSprite;
            return false;
		}

		resourceAmount -= amount;
		ResourceManager.ins.AddResource(ResourceType, amount);
		UpdateSize();

		return true;
	}


	private void Regenerate() {
		if (!IsDepleted && resourceAmount < MaxResourceAmount) {
			resourceAmount++;
			UpdateSize();
        }
	}
}
