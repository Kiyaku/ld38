﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceBackground : MonoBehaviour {
	public SpriteRenderer BackgroundRenderer;
	public float MaxDist = 10;

	private void Update() {
		Color col = BackgroundRenderer.color;
		col.a = (Vector2.Distance(GameManager.ins.DefaultPlanet.transform.position, Camera.main.transform.position) - GameManager.ins.DefaultPlanet.Radius() + 0.5f) / MaxDist;
		col.a = Mathf.Clamp(col.a, 0, 1);
        BackgroundRenderer.color = col;
    }
}