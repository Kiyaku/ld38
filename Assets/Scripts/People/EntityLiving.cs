﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityLiving : MonoBehaviour, IAdjustable {
	public int MovementSpeed = 1;
	public int MaxHealth = 1;
	public int CurrentHealth {
		get;
		private set;
	}
	public Transform SpritePtr;

	protected Animator animator;


	public Planet planet {
		get;
		protected set;
	}

	private void Start() {
		transform.localScale = Vector3.one * Random.Range(0.7f, 1f);
		animator = SpritePtr.GetComponent<Animator>();
    }


	public virtual void AssignPlanet(Planet p) {
		planet = p;
		p.AddToContainer(transform);

		Vector3 direction = transform.position - p.transform.position;
		direction.Normalize();
		transform.position = p.transform.position + (direction * p.Radius());

		Vector3 d = (this.transform.position - planet.transform.position);
		d = planet.transform.InverseTransformDirection(d);
		transform.rotation = Quaternion.Euler(0, 0, Mathf.Atan2(d.y, d.x) * Mathf.Rad2Deg - 90);

		StartCoroutine(Move());
    }


	protected virtual IEnumerator Move() {
		float sin = 0;
		int hops = 0;
		float timer = 0;

		while (true) {
			yield return new WaitForSeconds(Random.Range(1, 4));

			hops = Random.Range(3, 10);
			int direction = Random.Range(0, 100) > 50 ? 1 : -1;

			for (int i = 0; i < hops; i++) {
				timer = 0;
				sin = 0;

				while (timer < 1) {
					timer += Time.deltaTime;
					sin = timer * 180 * 4;

					if (animator != null) {
						animator.SetBool("isWalking", true);
                    }

					SpritePtr.localScale = new Vector3(direction, 1, 1);

					transform.RotateAround(planet.transform.position, Vector3.back, direction * Time.deltaTime * (MovementSpeed / planet.Radius()));
					yield return null;
				}
			}

			if (animator != null) {
				animator.SetBool("isWalking", false);
			}
		}
	}


	public void Adjust() {
		Helper.AlignWithPlanet(planet, transform);
	}
}
