﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponTorpedo : Projectile {
	public float Speed = 1;
	public float MaxAcceleration;
	public int Lifespan = 3;

	private float acceleration = 0;


	public override void Activate() {
		IsActive = true;
		acceleration = 2;
		Invoke("Suicide", Lifespan);
	}


	private void Update() {
		if (IsActive) {
			if (target != null) {
				transform.rotation = Quaternion.RotateTowards(transform.rotation, Helper.LookAt(transform, target), Time.deltaTime * 200f);
			} 

			acceleration += Time.deltaTime * Speed;
			acceleration = Mathf.Clamp(acceleration, 0, MaxAcceleration);

			transform.Translate(Vector3.up * acceleration * Time.deltaTime);
		}
	}

	private void Suicide() {
		Destroy(gameObject);
	}


	protected override void BlowUp(Transform target) {
		IDestructable destructable = target.GetComponent<IDestructable>();

		if (destructable != null) {
			destructable.DealDamage(Damage);
		}

		Destroy(gameObject);
	}
}
