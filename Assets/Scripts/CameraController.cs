﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
	[SerializeField]
	private Transform target;

	public float MovementSpeed = 1;


	public void SetTarget(Transform target) {
		this.target = target;
	}


	private void LateUpdate() {
		if (target != null) {
			Vector3 newPos = target.position;
			newPos.z = transform.position.z;
			transform.position = Vector3.Lerp(transform.position, newPos, Time.deltaTime * MovementSpeed);

			if (target.gameObject.activeSelf) {
				transform.rotation = Quaternion.Lerp(transform.rotation, target.rotation, Time.deltaTime * MovementSpeed);
			}
		}
	}
}
