﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour {
	[System.Serializable]
	public class Info {
		public Projectile ProjectilePrefab;
		public float CoolDown = 1;
		public float Damage = 1;
	}

	public Info WeaponInfo;

	private float coolDown = 0;


	public void Shoot(Vector2 pos) {
		if (coolDown <= 0 && WeaponInfo != null) {
			Projectile pro = Instantiate(WeaponInfo.ProjectilePrefab);
			pro.transform.position = transform.position;
			pro.transform.rotation = transform.rotation;
			pro.SetTarget(pos);
			//pro.transform.rotation = Helper.LookAt(pro.transform, pos);
			pro.Activate();
			coolDown = WeaponInfo.CoolDown;
        }
	}


	private void Update() {
		if (coolDown > 0) {
			coolDown -= Time.deltaTime;
		}
	}
}
