﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Helper {
	public static Quaternion LookAt(Transform transform, Vector3 pos) {
		Vector3 diff = pos - transform.position;
		diff.Normalize();

		float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
		return Quaternion.Euler(0f, 0f, rot_z - 90);
	}


	public static void AlignWithPlanet(Planet p, Transform t, float extraDist = 0) {
		t.position = GetPositionOnPlanet(p, t, extraDist);

		Vector3 d = (t.position - p.transform.position);
		d = p.transform.InverseTransformDirection(d);
		t.rotation = Quaternion.Euler(0, 0, Mathf.Atan2(d.y, d.x) * Mathf.Rad2Deg - 90);
	}


	public static Vector3 GetPositionOnPlanet(Planet p, Transform t, float extraDist = 0) {
		Vector3 direction = t.position - p.transform.position;
		direction.Normalize();
		return p.transform.position + (direction * (p.Radius() + extraDist));
	}


	public static string NumberToSimpleRoman(int num) {
		string[] numbers = new string[] { "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X"};

		if (num != 0 && numbers.Length >= num) {
			return numbers[num - 1];
        }

		return "";
	}
}
