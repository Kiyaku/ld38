﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugGUI : MonoBehaviour {
	public bool DisplayResources = true;
	public bool DisplayTime = true;


	private void OnGUI() {
		if (DisplayResources) {
			Rect rect = new Rect(Screen.width - 150, 10, 150, 20);

			foreach (Resource r in ResourceManager.ins.Resources) {
				GUI.Label(rect, r.Name + ": " + r.CurrentValue + " / " + r.MaxValue);
				rect.y += 20;
            }
		}

		if (DisplayTime) {
			Rect rect = new Rect(Screen.width / 2f - 250, 10, 500, 20);

			GUI.Label(rect, "Day: " + GameManager.ins.CurrentDay + " | Year: " + GameManager.ins.CurrentYear);
		}
	}
}
