﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PollutionColorizer : MonoBehaviour {
	public enum ObjType {
		CAMERA,
		SPRITE
	}
	public Color ColorNormal;
	public Color ColorPolluted;
	public ObjType Type;

	private SpriteRenderer spriteRenderer;
	private Camera cam;


	private void Awake() {
		cam = GetComponent<Camera>();
		spriteRenderer = GetComponent<SpriteRenderer>();
	}


	private void OnEnable() {
		GameManager.OnDayChanged += LerpColor;
	}


	private void OnDisable() {
		GameManager.OnDayChanged -= LerpColor;
	}


	private void LerpColor() {
		Color newCol = Color.Lerp(ColorNormal, ColorPolluted, GameManager.ins.GetPollutionPercent());

		switch (Type) {
			case ObjType.CAMERA:
				if (cam != null) cam.backgroundColor = newCol;
				break;

			case ObjType.SPRITE:
				if (spriteRenderer != null) spriteRenderer.color = newCol;
				break;
		}
	}
}
