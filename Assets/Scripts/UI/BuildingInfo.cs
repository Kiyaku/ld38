﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BuildingInfo : UIWindow {
	public static BuildingInfo ins;

	public TextMeshProUGUI BuildingNameText;
	public TextMeshProUGUI BuildingCurrentStatsText;
	public TextMeshProUGUI BuildingNextStatsText;
	public Button UpgradeButton;
	public AudioClip DestroyClip;

	private Building building;


	protected override void Awake() {
		base.Awake();

		ins = this;
	}


	private void OnEnable() {
		Building.OnResourceChanged += RefreshInfo;
	}


	private void OnDisable() {
		Building.OnResourceChanged -= RefreshInfo;
	}


	private void RefreshInfo() {
		SetInfo(building, false);
	}


	public void SetInfo(Building building, bool openAutomatically = true) {
		if (building == null)
			return;
		
        this.building = building;
		
		BuildingNameText.text = string.Format("{0} | Tier {1}", building.Name, building.Tier);

		Building.TierInfo currentInfo = building.GetTierInfo(building.Tier);
		Building.TierInfo nextInfo = building.GetTierInfo(building.Tier + 1);

		BuildingCurrentStatsText.text = "<b>CURRENT STATS</b>";

		foreach (Resource r in currentInfo.Stats) {
			if (r.Type == Resource.ResourceTypes.POPULATION) {
				BuildingCurrentStatsText.text += string.Format("\n{0}: {1} / {2}", r.Name, r.CurrentValue, r.MaxValue);
			} else {
				BuildingCurrentStatsText.text += string.Format("\n{0}: {1}", r.Name, r.MaxValue);
			}
		}

		if (nextInfo != null) {
			UpgradeButton.interactable = true;
			BuildingNextStatsText.text = "<b>NEXT TIER:</b>";

			foreach (Resource r in nextInfo.Stats) {
				BuildingNextStatsText.text += string.Format("\n{0}: {1}", r.Name, r.MaxValue);
			}

			BuildingNextStatsText.text += "\n\n<b>Upgrade Cost:</b>";

			foreach (Resource r in nextInfo.Cost) {
				if (ResourceManager.ins.GetResourceByType(r.Type).CurrentValue < r.MaxValue) {
					BuildingNextStatsText.text += "<color=#FF4242>";
				}

				BuildingNextStatsText.text += string.Format("\n{0}: {1}", ResourceManager.ins.GetNameForResource(r.Type), r.MaxValue);

				if (ResourceManager.ins.GetResourceByType(r.Type).CurrentValue < r.MaxValue) {
					BuildingNextStatsText.text += "</color>";
				}
			}

		} else {
			UpgradeButton.interactable = false;
			BuildingNextStatsText.text = "";
        }

		if (openAutomatically)
			Show();
    }


	public void DeleteBuilding() {
		for (int i = 1; i <= building.Tier; i++) {
			foreach (Resource r in building.GetTierInfo(i).Cost) {
                ResourceManager.ins.AddResource(r.Type, r.MaxValue / 2);
			}
		}

		GameManager.ins.SFXPlayer.PlayOneShot(DestroyClip);
        building.DestroyBuilding();
		building = null;
		Hide();
	}


	public void UpgradeBuilding() {
		if (building == null)
			return;

		Building.TierInfo nextTierInfo = building.GetTierInfo(building.Tier + 1);

		if (nextTierInfo == null)
			return;

		if (BuildingManager.ins.PayForBuilding(building, building.Tier + 1)) {
			building.Upgrade();
			SetInfo(building);
		}
	}
}
