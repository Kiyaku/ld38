﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ResourceDisplay : MonoBehaviour {
	public TextMeshProUGUI ResourceText;
	public Resource.ResourceTypes ResourceType;
	public string TextTemplate;


	private void Update() {
		if (ResourceManager.ins != null) {
			ResourceText.text = string.Format(TextTemplate, ResourceManager.ins.GetResourceByType(ResourceType).CurrentValue, ResourceManager.ins.GetResourceByType(ResourceType).MaxValue);
		}
    }
}
