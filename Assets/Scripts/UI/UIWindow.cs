﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(CanvasGroup))]
public class UIWindow : MonoBehaviour {
	private CanvasGroup group;
	public bool IsOpen {
		get;
		private set;
	}


	protected virtual void Awake() {
		group = GetComponent<CanvasGroup>();
		Hide();
	}


	public virtual void Show() {
		if (group != null) {
			IsOpen = true;
			group.alpha = 1;
			group.interactable = true;
			group.blocksRaycasts = true;
		}
	}


	public virtual void Hide() {
		if (group != null) {
			IsOpen = false;
			group.alpha = 0;
			group.interactable = false;
			group.blocksRaycasts = false;
		}
	}


	public virtual void Toggle() {
		if (IsOpen)
			Hide();
		else
			Show();
	}
}
