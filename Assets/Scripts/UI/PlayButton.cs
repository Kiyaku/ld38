﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayButton : MonoBehaviour {
	public void LoadScene() {
		UnityEngine.SceneManagement.SceneManager.LoadScene("playground");
	}
}
