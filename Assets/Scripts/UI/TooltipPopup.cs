﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TooltipPopup : MonoBehaviour {
	public static TooltipPopup ins;
	public TextMeshProUGUI ToolTipText;
	public GameObject Group;

	private RectTransform rect;
	public Vector2 DefaultSize {
		get;
		private set;
	}


	private void Awake() {
		ins = this;
		rect = GetComponent<RectTransform>();
		DefaultSize = rect.sizeDelta;

		HideTooltip();
    }


	public void ShowTooltip(Vector3 pos, Vector2 size, string toolTipText) {
		rect.position = pos;
		rect.sizeDelta = size;
        ToolTipText.text = toolTipText;
		Group.SetActive(true);
    }


	public void HideTooltip() {
		Group.SetActive(false);
	}
}
