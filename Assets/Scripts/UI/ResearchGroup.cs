﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResearchGroup : MonoBehaviour {
	public RectTransform Line;


	public void SetLineLength(float length) {
		Line.sizeDelta = new Vector2(length, Line.sizeDelta.y);
	}
}
