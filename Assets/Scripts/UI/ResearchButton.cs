﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class ResearchButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {
	public TextMeshProUGUI ButtonLabel;
	public Sprite ImageResearched;
	public Sprite ImageResearching;
	public Slider ProgressBar;
	public ResearchManager.ResearchID id {
		get;
		private set;
	}
	public int tier {
		get;
		private set;
	}

	private Button button;
	private ResearchManager.ResearchTier assignedTier;
	private int daysToResearch = 0;


	private void Awake() {
		button = GetComponent<Button>();
	}


	public void SetInfo(string name, bool available, ResearchManager.ResearchID id, int tier, ResearchManager.ResearchTier assignedTier) {
		this.id = id;
		this.tier = tier;
		this.assignedTier = assignedTier;
        ButtonLabel.text = name;

		if (assignedTier.Researched) {
			button.image.sprite = ImageResearched;
		} else if (assignedTier.isResearching) {
			button.image.sprite = ImageResearching;
		}

		button.interactable = available;
    }


	public void DoResearch() {
		if (assignedTier.Researched || assignedTier.isResearching)
			return;

        ResearchManager.Research research = ResearchManager.ins.Researchs.Find((r) => r.ID == id);

		if (research != null) {
			if (ResourceManager.ins.PayResources(research.Tiers[tier - 1].Cost)) {
				daysToResearch = assignedTier.DaysToResearch;
                assignedTier.isResearching = true;
				GameManager.OnDayChanged += ProgressResearch;
				ResearchWindow.ins.UpdateInfo();
				ResearchManager.ins.ResearchUpdated();
            }
		}
    }


	private void ProgressResearch() {
		daysToResearch--;

		ProgressBar.value = 1f - (float)daysToResearch / (float)assignedTier.DaysToResearch;

		if (daysToResearch <= 0) {
			assignedTier.Researched = true;
			ResearchWindow.ins.UpdateInfo();
			ResearchManager.ins.ResearchUpdated();
			GameManager.OnDayChanged -= ProgressResearch;
			ProgressBar.value = 1;
			ResearchToast.ToastInfo info = new ResearchToast.ToastInfo();
			info.Text = string.Format("{0} {1} completed", ResearchManager.ins.GetResearchByID(id).Name, Helper.NumberToSimpleRoman(tier));
			info.Icon = null;
			ResearchToast.ins.AddToast(info);
        }
	}


	public void OnPointerEnter(PointerEventData eventData) {
		ShowTooltip();
	}


	public void OnPointerExit(PointerEventData eventData) {
		HideTooltip();
	}


	public void ShowTooltip() {
		string tooltip = string.Format("<size=-10>{0}\n\n", assignedTier.Description);

		if (!assignedTier.Researched && !assignedTier.isResearching) {
			tooltip += string.Format("Days to research: {0}\n", assignedTier.DaysToResearch);

			foreach (Resource r in assignedTier.Cost) {
				if (ResourceManager.ins.GetResourceByType(r.Type).CurrentValue < r.MaxValue) {
					tooltip += "<color=#FF4242>";
				}

				tooltip += string.Format("\n{0}: \t{1}", ResourceManager.ins.GetNameForResource(r.Type), r.MaxValue);

				if (ResourceManager.ins.GetResourceByType(r.Type).CurrentValue < r.MaxValue) {
					tooltip += "</color>";
				}
			}
		}

		tooltip += "</size>";

		Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		pos = RectTransformUtility.WorldToScreenPoint(Camera.main, pos);

		TooltipPopup.ins.ShowTooltip(transform.position + new Vector3(160, 0), new Vector2(500, 300), tooltip);
	}


	public void HideTooltip() {
		TooltipPopup.ins.HideTooltip();
	}
}
