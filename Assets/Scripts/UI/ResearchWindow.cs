﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ResearchWindow : UIWindow {
	public static ResearchWindow ins;

	public ResearchButton ResearchButtonPrefab;
	public ResearchGroup ResearchGroupPrefab;
	public Image ResearchIconPrefab;
	public Transform Anchor;

	private List<ResearchButton> buttons = new List<ResearchButton>();


	protected override void Awake() {
		ins = this;
		base.Awake();
	}


	public override void Show() {
		base.Show();

		BuildingInfo.ins.Hide();

		if (buttons.Count == 0) {
			foreach (ResearchManager.Research r in ResearchManager.ins.Researchs) {
				ResearchGroup group = Instantiate(ResearchGroupPrefab);
				group.transform.SetParent(Anchor, false);

				Image icon = Instantiate(ResearchIconPrefab);
				icon.transform.SetParent(group.transform, false);
				icon.sprite = r.Icon;

				int i = 0;

				foreach (var tier in r.Tiers) {
					ResearchManager.ResearchTier previousTier = null;

					if (i > 0) {
						previousTier = r.Tiers[i - 1];
                    }

					ResearchButton button = Instantiate(ResearchButtonPrefab);
					button.transform.SetParent(group.transform);
					button.SetInfo(r.Name + " " + Helper.NumberToSimpleRoman(i + 1), previousTier != null ? previousTier.Researched : true, r.ID, i + 1, tier);
					button.transform.localScale = Vector3.one;
					buttons.Add(button);
					i++;
				}
			}
		} else {
			UpdateInfo();
        }
	}


	public void UpdateInfo() {
		foreach (ResearchManager.Research r in ResearchManager.ins.Researchs) {
			int i = 0;

			foreach (var tier in r.Tiers) {
				ResearchManager.ResearchTier previousTier = null;

				if (i > 0) {
					previousTier = r.Tiers[i - 1];
				}

				ResearchButton button = buttons.Find((b) => b.id == r.ID && b.tier == i + 1);

				if (button != null) {
					button.SetInfo(r.Name + " " + Helper.NumberToSimpleRoman(i + 1), previousTier != null ? previousTier.Researched : true, r.ID, i + 1, tier);
				} else{
					Debug.LogWarning("Button is null");
				}

				i++;
			}
		}
	}
}
