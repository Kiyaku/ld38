﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ResearchToast : MonoBehaviour {
	public static ResearchToast ins;

	public struct ToastInfo {
		public string Text;
		public Sprite Icon;
	}

	public TextMeshProUGUI ToastText;
	public Image ToastIcon;

	private Queue<ToastInfo> researchQueue = new Queue<ToastInfo>();
	private bool isAnimating = false;
	private Vector3 originalPos;
	private Vector3 targetPos;


	private void Awake() {
		ins = this;
		originalPos = transform.position;
		targetPos = originalPos + new Vector3(0, 120, 0);
    }


	private void Start() {
	}


	public void AddToast(ToastInfo info) {
		researchQueue.Enqueue(info);
		CheckQueue();
    }


	private void CheckQueue() {
		if (researchQueue.Count > 0 && !isAnimating) {
			StartCoroutine(AnimateQueue());
		}
	}


	private IEnumerator AnimateQueue() {
		isAnimating = true;
		float timer = 0;

		ToastInfo info = researchQueue.Dequeue();
        ToastText.text = info.Text;
		ToastIcon.sprite = info.Icon;

		while (timer < 1) {
			timer += Time.deltaTime * 2f;

			transform.position = Vector3.Lerp(originalPos, targetPos, Mathf.SmoothStep(0, 1, timer));

			yield return null;
		}

		yield return new WaitForSeconds(2);

		timer = 1;

		while (timer > 0) {
			timer -= Time.deltaTime * 2f;

			transform.position = Vector3.Lerp(originalPos, targetPos, Mathf.SmoothStep(0, 1, timer));

			yield return null;
		}

		isAnimating = false;
		CheckQueue();
	}
}
