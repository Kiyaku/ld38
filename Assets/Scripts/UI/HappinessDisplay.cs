﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HappinessDisplay : MonoBehaviour {
	public TextMeshProUGUI ResourceText;

	private void Update() {
		Color col = Color.white;

		/*
		if (GameManager.ins.Happiness > 50) {
			col = Color.Lerp(Color.white, Color.green, ((GameManager.ins.Happiness - 50) / 100f) * 2f);
		} else {
			col = Color.Lerp(Color.red, Color.white, (GameManager.ins.Happiness * 2f) / 100f);
		}
		*/
		
        ResourceText.text = string.Format("<color=#{0}>Happiness\n{1}%</color>", ColorUtility.ToHtmlStringRGB(col), GameManager.ins.Happiness);
	}
}
