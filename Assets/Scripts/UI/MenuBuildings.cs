﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuBuildings : MonoBehaviour {
	public static MenuBuildings ins;

	public ButtonBuilding ButtonBuildingPrefab;
	public Transform BuildingButtonAnchor;

	private List<ButtonBuilding> buttons = new List<ButtonBuilding>();
	private CanvasGroup canvasGroup;
	private bool isOpen = true;


	private void Awake() {
		ins = this;
		canvasGroup = GetComponent<CanvasGroup>();
	}


	private void Start() {
		Toggle();
	}


	public void Toggle() {
		if (isOpen) {
			Hide();
		} else {
			Show();

			if (buttons.Count == 0) {
				foreach (var building in GameManager.ins.Buildings) {
					ButtonBuilding button = Instantiate(ButtonBuildingPrefab);
					button.transform.SetParent(BuildingButtonAnchor, false);
					button.SetInfo(building);
					buttons.Add(button);
				}
			}
		}
	}


	public void UpdateButtons() {
		if (buttons.Count > 0) {
			foreach (var button in buttons) {
				button.RefreshInfo();
			}
		}
	}


	private void Show() {
		isOpen = true;
        canvasGroup.alpha = 1;
		canvasGroup.interactable = true;
		canvasGroup.blocksRaycasts = true;
		UpdateButtons();
    }


	private void Hide() {
		isOpen = false;
        canvasGroup.alpha = 0;
		canvasGroup.interactable = false;
		canvasGroup.blocksRaycasts = false;
	}
}
