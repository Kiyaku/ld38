﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlanetPointer : MonoBehaviour {
	public Image Arrow;
	public TextMeshProUGUI Text;
	public Transform AssignedTarget;

	private void Update() {
		Vector3 screenCenter = new Vector3(Screen.width / 2, Screen.height / 2, 0);

		if (SpaceshipController.ins.IsFlying) {
			Text.enabled = true;
			Vector3 dir = (AssignedTarget.position - SpaceshipController.ins.transform.position).normalized * 200f;
			Quaternion offsetRot = Quaternion.Euler(0, 0, -Camera.main.transform.eulerAngles.z);
			Text.transform.localPosition = Quaternion.Euler(0, 0, -Camera.main.transform.eulerAngles.z) * dir;
			//Arrow.transform.localRotation = Quaternion.LookRotation(Vector3.forward, dir);
        } else {
			Text.enabled = false;
		}
	}
}
