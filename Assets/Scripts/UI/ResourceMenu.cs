﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ResourceMenu : UIWindow {
	public TextMeshProUGUI ResouceText;
	private RectTransform rect;
	private List<Resource.ResourceTypes> discoveredTypes = new List<Resource.ResourceTypes>();


	protected override void Awake() {
		base.Awake();

		rect = GetComponent<RectTransform>();
	}


	private void OnEnable() {
		ResourceManager.OnResourceUpdated += UpdateInfo;
	}


	private void OnDisable() {
		ResourceManager.OnResourceUpdated -= UpdateInfo;
	}


	private void Start() {
		Show();
	}


	private void UpdateInfo(Resource res) {
		ResouceText.text = "";
		int lines = 0;

		foreach (Resource r in ResourceManager.ins.Resources) {
			if (!discoveredTypes.Contains(r.Type) && r.CurrentValue > 0) {
				discoveredTypes.Add(r.Type);
			}

			if (discoveredTypes.Contains(r.Type)) {
				ResouceText.text += string.Format("<b>{0}:</b> {1}\n", r.Name, r.CurrentValue);
				lines++;
			}
		}

		rect.sizeDelta = new Vector2(rect.sizeDelta.x, lines * 37 + 8);
	}
}
