﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlignTextWithWorldObject : MonoBehaviour {
	public Transform WorldObj;


	private void LateUpdate() {
		Vector3 screenPos = Camera.main.WorldToScreenPoint(WorldObj.position);
		transform.position = screenPos;
	}
}
