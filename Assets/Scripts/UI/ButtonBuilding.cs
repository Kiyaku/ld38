﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class ButtonBuilding : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {
	public GameManager.BuildingInfo AssignedBuilding {
		get;
		private set;
	}


	public TextMeshProUGUI BuildingNameLabel;
	public Image BuildingImage;

	private Button button;
	private RectTransform rect;


	private void Awake() {
		button = GetComponent<Button>();
		rect = GetComponent<RectTransform>();
	}


	public void SetInfo(GameManager.BuildingInfo building) {
		AssignedBuilding = building;
		BuildingNameLabel.text = building.BuildingPtr.Name;
		BuildingImage.sprite = building.BuildingImage;
		RefreshInfo();
	}


	public void RefreshInfo() {
		if (BuildingManager.ins.HasResources(AssignedBuilding.BuildingPtr, 1)) {
			button.interactable = true;
		} else {
			button.interactable = false;
		}
	}
	

	public void OnClick() {
		BuildingManager.ins.ShowPreview(AssignedBuilding);
	}


	public void OnPointerEnter(PointerEventData eventData) {
		ShowTooltip();
    }


	public void OnPointerExit(PointerEventData eventData) {
		HideTooltip();
	}


	public void ShowTooltip() {
		string tooltip = string.Format("<b>Building Cost</b>\n");

		Building.TierInfo info = AssignedBuilding.BuildingPtr.GetTierInfo(1);

		foreach (Resource r in info.Cost) {
			if (ResourceManager.ins.GetResourceByType(r.Type).CurrentValue < r.MaxValue) {
				tooltip += "<color=#FF4242>";
			}

			tooltip += string.Format("<size=-10>{0}: {1}\n</size>", ResourceManager.ins.GetNameForResource(r.Type), r.MaxValue);

			if (ResourceManager.ins.GetResourceByType(r.Type).CurrentValue < r.MaxValue) {
				tooltip += "</color>";
			}
		}

		if (!string.IsNullOrEmpty(AssignedBuilding.BuildingPtr.Description)) {
			tooltip += string.Format("\n<size=-15><i><color=#AAAAAA>{0}</color></i></size>", AssignedBuilding.BuildingPtr.Description);
        }

		Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		pos = RectTransformUtility.WorldToScreenPoint(Camera.main, pos);

		TooltipPopup.ins.ShowTooltip(rect.position + new Vector3(60, 60), new Vector2(300, 300), tooltip);
	}


	public void HideTooltip() {
		TooltipPopup.ins.HideTooltip();
	}
}
