﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cargo : MonoBehaviour {
	private int maxStorage = 3;
	private List<Transform> storage = new List<Transform>();
	private int[] storageUpgrades = new int[] { 2, 5, 10};
	private int extraUpgrade = 0;


	private void OnEnable() {
		ResearchManager.OnResearchUpdated += ResearchUpdated;
	}


	private void OnDisable() {
		ResearchManager.OnResearchUpdated -= ResearchUpdated;
	}


	private void ResearchUpdated() {
		int currentTier = ResearchManager.ins.GetCurrentTier(ResearchManager.ResearchID.SPACESHIP_3);

		if (currentTier > 0 && ResearchManager.ins.HasResearched(ResearchManager.ResearchID.SPACESHIP_3, currentTier)) {
			extraUpgrade = storageUpgrades[currentTier - 1];
		}
	}


	public bool AddToStorage(Transform t) {
		if (maxStorage + extraUpgrade > storage.Count) {
			storage.Add(t);
			return true;
		}

		return false;
	}


	public List<Transform> Unload() {
		List<Transform> temp = new List<Transform>();

		foreach (Transform t in storage) {
			if (t != null) {
				temp.Add(t);
			}
		}

		storage.Clear();

		return temp;
	}
}
