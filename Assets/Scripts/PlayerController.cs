﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : EntityLiving {
	public static PlayerController ins;

	private enum MovementTypes {
		PLANET,
		SPACESHIP
	}


	private MovementTypes currentMovement = MovementTypes.PLANET;

	[SerializeField]
	private bool AFK = true;
	private float sin = 0;
	private float bonusMovementSpeed = 0;
	private float[] bonusSpeeds = new float[] { 0.1f, 0.2f, 0.3f };


	private void Awake() {
		ins = this;
		animator = SpritePtr.GetComponent<Animator>();
    }


	private void OnEnable() {
		ResearchManager.OnResearchUpdated += ResearchUpdated;
	}


	private void OnDisable() {
		ResearchManager.OnResearchUpdated -= ResearchUpdated;
	}


	private void ResearchUpdated() {
		int currentTier = ResearchManager.ins.GetCurrentTier(ResearchManager.ResearchID.PLAYER_1);

		if (currentTier > 0 && ResearchManager.ins.HasResearched(ResearchManager.ResearchID.PLAYER_1, currentTier)) {
            bonusMovementSpeed = bonusSpeeds[currentTier - 1];
        }
    }


	private void Start() {
		AssignPlanet(GameManager.ins.DefaultPlanet);
	}


	public override void AssignPlanet(Planet p) {
		base.AssignPlanet(p);

		currentMovement = MovementTypes.PLANET;
	}


	private void Update() {
		if (currentMovement == MovementTypes.PLANET && GameManager.ins.IsGameRunning) {
			float dir = Input.GetAxis("Horizontal");

			if (AFK) dir = 1;

			animator.SetBool("isWalking", Mathf.Abs(dir) >= 0.1f);

			if (Mathf.Abs(dir) > 0.1f) {
				SpritePtr.localScale = new Vector3(Mathf.Sign(dir), 1, 1);
			}

			transform.RotateAround(planet.transform.position, Vector3.back, dir * Time.deltaTime * ((MovementSpeed * (1f + bonusMovementSpeed)) / planet.Radius()));
			AssignPlanet(planet);

			if (Input.GetKeyDown(GameConfig.USE_KEY)) {
				if (Vector2.Distance(transform.position, SpaceshipController.ins.transform.position) < 0.3f) {
					SpaceshipController.ins.MountShip(transform);
					currentMovement = MovementTypes.SPACESHIP;
				}
			}
		}
	}


	protected override IEnumerator Move() {
		yield return null;
	}
}
