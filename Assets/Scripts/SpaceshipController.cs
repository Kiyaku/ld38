﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceshipController : MonoBehaviour {
	public static SpaceshipController ins;

	private enum MovementTypes {
		IDLING,
		FLYING,
		LANDING
	}

	public float MovementSpeed = 1;
	public Drill CurrentDrill;
	public Weapon CurrentWeapon;
	public Cargo CargoPtr;
	public GameObject FlameGroup;
	public SpriteRenderer RocketRenderer;
	public AudioClip SFXLanding;
	public AudioClip SFXTakingOff;
	public Sprite[] RocketSprites = new Sprite[0];
	public GameObject[] RocketFlames = new GameObject[0];

	public bool isControlling {
		get;
		private set;
    }
	public bool IsFlying {
		get {
			return currentMovement == MovementTypes.FLYING;
		}
	}
	private float offset = 0.15f;
	private Transform mounter;
	private MovementTypes currentMovement = MovementTypes.IDLING;

	private float acceleration = 0;

	private float bonusMovementSpeed = 0;
	private float[] bonusSpeeds = new float[] { 0.05f, 0.15f, 0.25f };


	private void Awake() {
		ins = this;
		isControlling = false;
		FlameGroup.SetActive(false);
    }

	
	private void OnEnable() {
		ResearchManager.OnResearchUpdated += ResearchUpdated;
	}


	private void OnDisable() {
		ResearchManager.OnResearchUpdated -= ResearchUpdated;
	}


	private void ResearchUpdated() {
		int currentTier = ResearchManager.ins.GetCurrentTier(ResearchManager.ResearchID.SPACESHIP_2);

		if (currentTier > 0 && ResearchManager.ins.HasResearched(ResearchManager.ResearchID.SPACESHIP_2, currentTier)) {
			RocketRenderer.sprite = RocketSprites[currentTier - 1];
			RocketFlames[currentTier - 1].SetActive(true);
			bonusMovementSpeed = bonusSpeeds[currentTier - 1];
		}
	}


	public void ExitShip() {
		if (mounter != null) {
			isControlling = false;
			mounter.gameObject.SetActive(true);
			mounter = null;
			FlameGroup.SetActive(false);
			StopAllCoroutines();
		}
	}


	public void MountShip(Transform mounter) {
		if (this.mounter == null && mounter != null) {
			this.mounter = mounter;
			mounter.parent = transform;
            mounter.gameObject.SetActive(false);
			isControlling = true;
			acceleration = 1;
			StartCoroutine(FlyShip());
        }
	}


	private IEnumerator FlyShip() {
		while(Input.GetKey(GameConfig.USE_KEY)) {
			yield return null;
		}

		currentMovement = MovementTypes.FLYING;
		FlameGroup.SetActive(true);

		GameManager.ins.SFXPlayer.PlayOneShot(SFXTakingOff);

		while (currentMovement == MovementTypes.FLYING && GameManager.ins.IsGameRunning) {
			if (mounter) {
				Vector2 dir = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

				if (Mathf.Abs(dir.y) >= 0.1f) {
					acceleration += dir.y * Time.deltaTime * MovementSpeed;
				}

				acceleration = Mathf.Clamp(acceleration, -0.5f, 2);

				acceleration = Mathf.Lerp(acceleration, 0, Time.deltaTime * 0.4f);

				transform.Translate(new Vector3(0, (acceleration * (1f + bonusMovementSpeed)) * Time.deltaTime, 0));

				if (Mathf.Abs(dir.x) >= 0.1f) {
					transform.Rotate(Vector3.back, dir.x * Time.deltaTime * 150);
				}

				mounter.position = transform.position;

				if (Input.GetMouseButtonDown(0) && CurrentDrill != null) {
					Vector3 originPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
					originPos.z = -5;

					RaycastHit2D[] hits = Physics2D.RaycastAll(originPos, Vector3.forward, 10);

					foreach (var hit in hits) {
						if (hit.collider != null && hit.collider.GetComponent<IMineable>() != null) {
							CurrentDrill.Mine(hit.transform);
							break;
                        }
					}
				}

				if (!Input.GetMouseButton(0) && CurrentDrill != null && CurrentDrill.IsDrilling) {
					CurrentDrill.Stop();
				}

				if (Input.GetMouseButtonDown(1) && CurrentWeapon != null) {
					CurrentWeapon.Shoot(Camera.main.ScreenToWorldPoint(Input.mousePosition));
                }
			}

			yield return null;
		}
	}


	private void OnTriggerEnter2D(Collider2D col) {
		// Landing
		if (!isControlling)
			return;

		if (col.GetComponent<Planet>() && col.GetComponent<Planet>() == GameManager.ins.DefaultPlanet) {
			Planet planet = col.GetComponent<Planet>();

			if (Vector2.Distance(transform.position, planet.transform.position) >= planet.Radius() - 0.1f && Vector2.Distance(transform.position, planet.transform.position) >= planet.Radius() + 0.1f) {
				if (currentMovement == MovementTypes.FLYING) {
					StartCoroutine(Landing(planet));
				}
			}
		} else if (col.GetComponent<PlanetDebris>()) {
			PlanetDebris debris = col.GetComponent<PlanetDebris>();
			debris.PickUp(CargoPtr);
        }
	}


	private void Update() {
		if (currentMovement == MovementTypes.IDLING)
			AlignWithPlanet(PlayerController.ins.planet);
    }


	private void AlignWithPlanet(Planet p) {
		Vector3 direction = transform.position - p.transform.position;
		direction.Normalize();
		transform.position = p.transform.position + (direction * (p.Radius() + offset));

		Vector3 d = (this.transform.position - p.transform.position);
		d = p.transform.InverseTransformDirection(d);
		transform.rotation = Quaternion.Euler(0, 0, Mathf.Atan2(d.y, d.x) * Mathf.Rad2Deg - 90);
	}


	private IEnumerator Landing(Planet p) {
		float timer = 0;

		currentMovement = MovementTypes.LANDING;

		GameManager.ins.SFXPlayer.PlayOneShot(SFXLanding);

		isControlling = false;
		acceleration = 0;

		Vector3 originalPos = transform.position;
		Vector3 direction = transform.position - p.transform.position;
		direction.Normalize();
		Vector3 position = p.transform.position + (direction * (p.Radius() + offset));

		Quaternion originalRotation = transform.rotation;
		Vector3 d = (this.transform.position - p.transform.position);
		d = p.transform.InverseTransformDirection(d);
		Quaternion rotation = Quaternion.Euler(0, 0, Mathf.Atan2(d.y, d.x) * Mathf.Rad2Deg - 90);

		while (timer < 1) {
			timer += Time.deltaTime;

			transform.position = Vector3.Lerp(originalPos, position, timer);
			transform.rotation = Quaternion.Lerp(originalRotation, rotation, timer);

			yield return null;
		}

		transform.position = position;
		transform.rotation = rotation;

		ExitShip();
		PlayerController.ins.AssignPlanet(p);
		DeliverCargo();

		currentMovement = MovementTypes.IDLING;
	}


	private void DeliverCargo() {
		List<Transform> cargo = CargoPtr.Unload();

		if (cargo.Count > 0) {
			PlayerController.ins.planet.Grow(cargo.Count * 0.005f);

			foreach (Transform t in cargo) {
				Destroy(t.gameObject);
			}
		}
	}
}
