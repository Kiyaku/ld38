﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Resource {
	public enum ResourceTypes {
		WOOD,
		STONE,
		WATER,
		FOOD,
		POPULATION,
		SPACE1,
		SPACE2,
		SPACE3,
		SPACE4,
		SPACE5
	}

	public Resource(ResourceTypes type, int maxValue) {
		this.Type = type;
		this.MaxValue = maxValue;
	}

	public string Name = "UNKNOWN";
	public ResourceTypes Type;
	public int CurrentValue {
		get;
		private set;
	}
	public int MaxValue = 0;


	public int AddValue(int val) {
		int added = 0;

		if (val + CurrentValue > MaxValue) {
			added = val + CurrentValue - MaxValue;
		} else {
			added = val;
		}

		CurrentValue += val;
		CurrentValue = Mathf.Clamp(CurrentValue, 0, MaxValue);

		return added;
	}


	public void SetValue(int val) {
		CurrentValue = val;
		CurrentValue = Mathf.Clamp(CurrentValue, 0, MaxValue);
	}
}
