﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDestructable {
	void DestroyObject();
	void DealDamage(int amount);
}
