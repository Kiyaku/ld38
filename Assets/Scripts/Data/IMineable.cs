﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMineable {
	bool Mine(Drill.Info drill);
}
