﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Projectile : MonoBehaviour {
	public abstract void Activate();
	public void SetTarget(Vector3 target) {
		this.target = target;
	}

	public bool IsActive = false;
	public int Damage = 1;
	protected Vector3 target;


	protected void OnTriggerEnter2D(Collider2D col) {
		BlowUp(col.transform);
	}


	protected abstract void BlowUp(Transform target);
}
