﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PassiveMiner : MonoBehaviour {
	public Dictionary<Resource.ResourceTypes, int> ResourcesPerDay = new Dictionary<Resource.ResourceTypes, int>();

	private void Awake() {
		ResourcesPerDay.Add(Resource.ResourceTypes.STONE, 1);
		ResourcesPerDay.Add(Resource.ResourceTypes.WOOD, 1);

		GameManager.OnDayChanged += Mine;
	}


	private void Mine() {
		foreach (var kvp in ResourcesPerDay) {
			ResourceManager.ins.AddResource(kvp.Key, kvp.Value);
		}
	}
}
