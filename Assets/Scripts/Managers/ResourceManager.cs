﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceManager : MonoBehaviour {
	public static ResourceManager ins;

	public List<Resource> Resources = new List<Resource>();
	public delegate void ResourceDelegate(Resource res);
	public static event ResourceDelegate OnResourceUpdated;


	private void Awake() {
		ins = this;
	}


	public Resource GetResourceByType(Resource.ResourceTypes type) {
		foreach (Resource r in Resources) {
			if (r.Type == type) {
				return r;
			}
		}

		return null;
	}


	// Returns how much has been added to the resource
	public int AddResource(Resource.ResourceTypes type, int val) {
		foreach (Resource r in Resources) {
			if (r.Type == type) {
				int dif = r.AddValue(val);

				if (OnResourceUpdated != null)
					OnResourceUpdated(r);

				return dif;
			}
		}

		return 0;
	}


	public bool HasResource(Resource.ResourceTypes type, int amount) {
		foreach (Resource r in Resources) {
			if (r.Type == type && r.CurrentValue >= amount) {
				return true;
			}
		}

		return false;
	}


	public bool HasResources(List<Resource> resources) {
		foreach (Resource r in resources) {
			if (!HasResource(r.Type, r.MaxValue)) {
				return false;
			}
		}

		return true;
	}


	public bool PayResources(List<Resource> resources) {
		if (HasResources(resources)) {
			foreach (Resource r in resources) {
				AddResource(r.Type, -r.MaxValue);
			}

			return true;
		}

		return false;
	}


	public string GetNameForResource(Resource.ResourceTypes type) {
		return Resources.Find((r) => r.Type == type).Name;
	}
}
