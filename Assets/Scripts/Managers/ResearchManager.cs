﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResearchManager : MonoBehaviour {
	public static ResearchManager ins;
	public static event System.Action OnResearchUpdated;

	public enum ResearchID {
		PLAYER_1,
		SPACESHIP_1,
		SPACESHIP_2,
		SPACESHIP_3,
		SPACESHIP_4,
		BUILDINGS_1,
		BUILDINGS_2,
	}


	private void Awake() {
		ins = this;

		foreach (Research research in Researchs) {
			for (int i = 0; i < research.Tiers.Count; i++) {
				switch (i) {
					case 0:
						research.Tiers[i].Cost = new List<Resource>();
						research.Tiers[i].Cost.Add(new Resource(Resource.ResourceTypes.SPACE1, 75));
						research.Tiers[i].Cost.Add(new Resource(Resource.ResourceTypes.SPACE2, 75));
						break;

					case 1:
						research.Tiers[i].Cost = new List<Resource>();
						research.Tiers[i].Cost.Add(new Resource(Resource.ResourceTypes.SPACE1, 150));
						research.Tiers[i].Cost.Add(new Resource(Resource.ResourceTypes.SPACE2, 150));
						research.Tiers[i].Cost.Add(new Resource(Resource.ResourceTypes.SPACE3, 75));
						research.Tiers[i].Cost.Add(new Resource(Resource.ResourceTypes.SPACE4, 75));
						break;

					case 2:
						research.Tiers[i].Cost = new List<Resource>();
						research.Tiers[i].Cost.Add(new Resource(Resource.ResourceTypes.SPACE1, 200));
						research.Tiers[i].Cost.Add(new Resource(Resource.ResourceTypes.SPACE2, 200));
						research.Tiers[i].Cost.Add(new Resource(Resource.ResourceTypes.SPACE3, 150));
						research.Tiers[i].Cost.Add(new Resource(Resource.ResourceTypes.SPACE4, 150));
						research.Tiers[i].Cost.Add(new Resource(Resource.ResourceTypes.SPACE5, 150));
						break;
				}
			}
		}
	}


	[System.Serializable]
	public class ResearchTier {
		public string Description;
		public List<Resource> Cost = new List<Resource>();
		public int DaysToResearch = 1;
		[HideInInspector]
		public int DayStarted = 0;
		[HideInInspector]
		public bool isResearching;
		public bool Researched;
	}


	[System.Serializable]
	public class Research {
		public ResearchID ID;
		public string Name;
		public string Description;
		public Sprite Icon;
		public List<ResearchTier> Tiers = new List<ResearchTier>();
	}
	

	public List<Research> Researchs = new List<Research>();


	public void ResearchUpdated() {
		if (OnResearchUpdated != null) {
			OnResearchUpdated();
		}
	}


	public Research GetResearchByID(ResearchID id) {
		return Researchs.Find((r) => r.ID == id);
    }


	public ResearchTier GetTier(ResearchID id, int tier) {
		Research r = GetResearchByID(id);

		if (r != null && r.Tiers.Count > tier - 1) {
			return r.Tiers[tier - 1];
		}

		return null;
    }


	public int GetCurrentTier(ResearchID id) {
		Research r = GetResearchByID(id);

		if (r != null) {
			int highestTier = 0;

			for (int i = 0; i < r.Tiers.Count; i++) {
				if (r.Tiers[i].Researched)
					highestTier = i;
            }

			return highestTier + 1;
		}

		return 0;
	}


	public bool HasResearched(ResearchID id, int tier) {
		ResearchTier info = GetTier(id, tier);

		if (info != null) {
			return info.Researched;
		}

		return false;
    }
}
