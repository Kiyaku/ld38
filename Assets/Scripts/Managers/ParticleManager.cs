﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleManager : MonoBehaviour {
	public static ParticleManager ins;

	[System.Serializable]
	public class ParticleGroup {
		public ParticleType Type;
		public ParticleSystem Particle;
	}

	public enum ParticleType {
		PLACING_SMOKE
	}


	public List<ParticleGroup> Particles = new List<ParticleGroup>();


	private void Awake() {
		ins = this;
	}


	public void PlayParticle(ParticleType type, Vector3 pos, Quaternion rotation) {
		ParticleGroup group = Particles.Find((p) => p.Type == type);

		if (group != null) {
			ParticleSystem system = Instantiate(group.Particle, pos, rotation);
		}
	} 
}
