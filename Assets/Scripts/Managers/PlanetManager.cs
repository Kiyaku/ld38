﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetManager : MonoBehaviour {
	public List<MineablePlanet> Planets = new List<MineablePlanet>();
	public int Distance = 10;
	public int Radius = 10;

	private void Start() {
		float halfDistance = Distance / 2f;
		int planetIndex = 0;

        for (int x = -Radius; x <= Radius; x++) {
			for (int y = -Radius; y <= Radius; y++) {
				if (x == 0 && y == 0)
					continue;

				if (Mathf.Abs(x) < 5 && Mathf.Abs(y) < 5) {
					planetIndex = Random.Range(0, 2);
				} else if (Mathf.Abs(x) < 12 && Mathf.Abs(y) < 12) {
					planetIndex = Random.Range(0, 5);
				} else {
					planetIndex = Random.Range(2, 5);
				}

				if (Random.Range(0, 100) > 50) {
					MineablePlanet p = Instantiate(Planets[planetIndex]);
					p.transform.position = new Vector3(x * Distance + Random.Range(-halfDistance, halfDistance), y * Distance + Random.Range(-halfDistance, halfDistance), 0);
					p.transform.SetParent(transform);
                }
			}
		}
    }
}
