﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {
	public static GameManager ins;

	[System.Serializable]
	public class BuildingInfo {
		public Building BuildingPtr;
		public Sprite BuildingImage;
	}

	public delegate void TimeDelegate();
	public static event TimeDelegate OnDayChanged;
	public static event TimeDelegate OnYearChanged;

	public List<BuildingInfo> Buildings = new List<BuildingInfo>();
	public List<Sprite> DebrisSprites = new List<Sprite>();

	public int CurrentDay {
		get;
		private set;
	}
	public int CurrentYear {
		get;
		private set;
	}
	public Planet DefaultPlanet;
	public EntityLiving CitizenPrefab;
	public int Pollution {
		get;
		private set;
	}
	public int MaxPollution {
		get;
		private set;
	}
	public int Happiness {
		get;
		private set;
	}
	public bool IsGameRunning {
		get;
		private set;
	}
	public AudioSource SFXPlayer;
	private float secondsPerDay = 2;
	private List<EntityLiving> citizens = new List<EntityLiving>();

	private float modHappiness = 0;
	private float modPollution = 0;
	private float modLivingSpace = 0;
	private float modNeeds = 0;


	private void Awake() {
		ins = this;

		IsGameRunning = true;
		Happiness = 50;
		CurrentDay = 1;
		CurrentYear = 1;
		StartCoroutine(IncreaseTime());

		MaxPollution = Mathf.FloorToInt(DefaultPlanet.Radius() * 100000);
    }


	private void Start() {
		ResourceManager.ins.AddResource(Resource.ResourceTypes.FOOD, 100);
		ResourceManager.ins.AddResource(Resource.ResourceTypes.WATER, 100);
		ResourceManager.ins.AddResource(Resource.ResourceTypes.WOOD, 100);
		ResourceManager.ins.AddResource(Resource.ResourceTypes.STONE, 100);

		ResourceManager.ins.AddResource(Resource.ResourceTypes.POPULATION, 1);
		SpawnCitizen();
		AdjustCitizens();
	}


	public Building GetBuildingByID(string id) {
		foreach (var building in Buildings) {
			if (building.BuildingPtr.ID == id) {
				return building.BuildingPtr;
			}
		}

		return null;
	}


	private void CalculateHappiness() {
        float homelessRate = (float)citizens.Count / (float)ResourceManager.ins.GetResourceByType(Resource.ResourceTypes.POPULATION).CurrentValue;
        modPollution = -(GetPollutionPercent() * 2f - 0.2f);

		if (homelessRate > 0.6f) {
			modLivingSpace -= Time.deltaTime * 2f;
		} else {
			modLivingSpace += homelessRate > 0.3f ? -Time.deltaTime : Time.deltaTime * 3f;
		}

		modPollution = Mathf.Clamp(modPollution, -1, 1);
		modLivingSpace = Mathf.Clamp(modLivingSpace, -1, 1);
		modNeeds = Mathf.Clamp(modNeeds, -1, 1);

		modHappiness = modPollution + modLivingSpace + modNeeds;
		modHappiness = Mathf.Clamp(modHappiness, -3, 3);
		//Debug.Log(string.Format("{0} | {1} | {2} | {3}", modHappiness, modPollution, modLivingSpace, modNeeds));

		Happiness += Mathf.FloorToInt(modHappiness);
		Happiness = Mathf.Clamp(Happiness, 0, 100);
    }


	private void IncreaseDay() {
		MaxPollution = Mathf.FloorToInt(DefaultPlanet.Radius() * 100000);

		if (CurrentDay % 5 == 0) {
			if (UnityEngine.Random.Range(0, 100) > 50) {
				ResourceManager.ins.AddResource(Resource.ResourceTypes.POPULATION, 1);
				SpawnCitizen();
				AdjustCitizens();
			}
		}

		if (OnDayChanged != null) {
			OnDayChanged();
		}

		ConsumeResources();
		CalculateHappiness();
	}


	public void IncreasePollution(int amount) {
		Pollution += amount / 2;

		if (Pollution > MaxPollution) {
			Pollution = MaxPollution;
        }
	}


	public float GetPollutionPercent() {
		return (float)Pollution / (float)MaxPollution;
	}


	public void SpawnCitizen() {
		Vector3 pos = new Vector3(0, 1, 0);
		EntityLiving entity = Instantiate(CitizenPrefab, DefaultPlanet.transform.position + pos, Quaternion.identity);
		entity.transform.RotateAround(DefaultPlanet.transform.position, Vector3.forward, UnityEngine.Random.Range(0, 360));
		entity.AssignPlanet(DefaultPlanet);
		citizens.Add(entity);
	}
	public void SpawnCitizen(Vector3 pos) {
		EntityLiving entity = Instantiate(CitizenPrefab, DefaultPlanet.transform.position + pos, Quaternion.identity);
		entity.AssignPlanet(DefaultPlanet);
		citizens.Add(entity);
	}


	private void RemoveCitizen() {
		if (citizens.Count > 0) {
			EntityLiving c = citizens[citizens.Count - 1];
			Destroy(c.gameObject);
			citizens.RemoveAt(citizens.Count - 1);
		}
	}


	private void ConsumeResources() {
		int neededFood = ResourceManager.ins.GetResourceByType(Resource.ResourceTypes.POPULATION).CurrentValue / 3;
		int neededWater = ResourceManager.ins.GetResourceByType(Resource.ResourceTypes.POPULATION).CurrentValue / 2;

		ResourceManager.ins.AddResource(Resource.ResourceTypes.FOOD, -neededFood);
		ResourceManager.ins.AddResource(Resource.ResourceTypes.WATER, -neededWater);

		if (ResourceManager.ins.HasResource(Resource.ResourceTypes.FOOD, neededFood)) {
			modNeeds += Time.deltaTime;
        } else {
			modNeeds -= Time.deltaTime;
		}

		if (ResourceManager.ins.HasResource(Resource.ResourceTypes.WATER, neededWater)) {
			modNeeds += Time.deltaTime;
		} else {
			modNeeds -= Time.deltaTime;
		}
	}


	public bool AdjustCitizens() {
		if (citizens.Count == 0)
			return true;

		List<Building> buildings = BuildingManager.ins.GetBuildingsOfType(Resource.ResourceTypes.POPULATION);

		foreach (var building in buildings) {
			House house = (House)building;

			while (house.HasRoom && citizens.Count > 0) {
				house.MoveIn();
				RemoveCitizen();
			}

			if (citizens.Count == 0) {
				return true;
			}
		}

		return false;
	}


	private IEnumerator IncreaseTime() {
		while (true) {
			yield return new WaitForSeconds(secondsPerDay);

			CurrentDay++;
			IncreaseDay();

			if (CurrentDay >= 365) {
				CurrentYear++;
				CurrentDay = 0;

				if (OnYearChanged != null) {
					OnYearChanged();
				}
			}
		}
	}
}
