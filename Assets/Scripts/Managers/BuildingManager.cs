﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingManager : MonoBehaviour {
	public static BuildingManager ins;

	public bool IsBuilding = false;
	public Material PreviewMaterial;
	public Color ColorOkay;
	public Color ColorBad;

	private Building buildingPreview;
	private List<Building> builtBuildings = new List<Building>();


	private void Awake() {
		ins = this;
	}


	public void ShowPreview(GameManager.BuildingInfo buildingInfo) {
		if (IsBuilding || SpaceshipController.ins.isControlling)
			return;

		IsBuilding = true;

		buildingPreview = Instantiate(buildingInfo.BuildingPtr);
		buildingPreview.GetComponent<SpriteRenderer>().material = PreviewMaterial;
		StartCoroutine(MovePreview());
	}


	private IEnumerator MovePreview() {
		Biome biome = null;
		Planet p = PlayerController.ins.planet;
		float heightOffset = 0.2f;

		while (Input.GetMouseButton(0)) {
			yield return null;
		}

		Vector3 pos = Vector3.zero;
		Vector3 direction = Vector3.zero;

		Material mat = buildingPreview.GetComponent<SpriteRenderer>().material;

		while (!Input.GetMouseButton(0)) {
			pos = Vector3.Lerp(pos, Camera.main.ScreenToWorldPoint(Input.mousePosition), Time.deltaTime * 5f);
			pos.z = 0;
			buildingPreview.transform.position = pos;

			direction = buildingPreview.transform.position - p.transform.position;
			direction.Normalize();
			buildingPreview.transform.position = p.transform.position + (direction * (p.Radius()));

			Vector3 d = (buildingPreview.transform.position - p.transform.position);
			d = p.transform.InverseTransformDirection(d);
			buildingPreview.transform.rotation = Quaternion.Lerp(buildingPreview.transform.rotation, Quaternion.Euler(0, 0, Mathf.Atan2(d.y, d.x) * Mathf.Rad2Deg - 90), Time.deltaTime * 10f);

			biome = buildingPreview.GetBiome(p);

			if (biome == null || biome.ResourceType != buildingPreview.Type || !HasResources(buildingPreview, 1) || BuildingInWay(buildingPreview.transform)) {
				mat.SetColor("_Color", ColorBad);
			} else {
				mat.SetColor("_Color", ColorOkay);
			}

			buildingPreview.transform.position = p.transform.position + (direction * (p.Radius() + heightOffset));

			if (Input.GetMouseButtonDown(1)) {
				Destroy(buildingPreview.gameObject);
				IsBuilding = false;
				yield break;
			}

			yield return null;
		}

		buildingPreview.transform.position = p.transform.position + (direction * (p.Radius()));

		if (biome != null && biome.ResourceType == buildingPreview.Type && !BuildingInWay(buildingPreview.transform) && PayForBuilding(buildingPreview, 1)) {
			Building building = Instantiate(GameManager.ins.GetBuildingByID(buildingPreview.ID));
			buildingPreview.transform.position = p.transform.position + (direction * (p.Radius() + heightOffset));
			building.transform.position = buildingPreview.transform.position;
			building.transform.rotation = buildingPreview.transform.rotation;
			building.PlaceBuilding(p);
			builtBuildings.Add(building);
			MenuBuildings.ins.UpdateButtons();
		}

		Destroy(buildingPreview.gameObject);

		IsBuilding = false;
	}


	private Building lastBuilding;

	private void Update() {
		if (!IsBuilding) {
			Vector3 originPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			originPos.z = -5;

			RaycastHit2D hit = Physics2D.Raycast(originPos, Vector3.forward, 10, 1 << LayerMask.NameToLayer("building"));

			if (hit.transform != null && hit.transform.GetComponent<Building>() != null) {
				if (lastBuilding != null) {
					lastBuilding.DontHighlight();
				}

				lastBuilding = hit.transform.GetComponent<Building>();
				lastBuilding.Highlight();

				if (Input.GetMouseButtonDown(0)) {
					SelectBuilding(hit.transform.GetComponent<Building>());
				}
			} else {
				if (lastBuilding != null) {
					lastBuilding.DontHighlight();
				}
			}
		}
    }


	private bool BuildingInWay(Transform b) {
		ContactFilter2D filter = new ContactFilter2D();
		filter.SetLayerMask(1 << b.gameObject.layer);
		Collider2D[] results = new Collider2D[1];
		b.GetComponent<Collider2D>().OverlapCollider(filter, results);

		foreach (Collider2D col in results) {
			if (col != null) {
				return true;
			}
		}

		return false;
	}


	public bool HasResources(Building building, int tier) {
		Building.TierInfo info = building.GetTierInfo(tier);

		if (info == null) {
			return false;
		}

		foreach (var resource in info.Cost) {
			if (ResourceManager.ins.GetResourceByType(resource.Type).CurrentValue < resource.MaxValue) {
				return false;
			}
		}

		return true;
	}


	public bool PayForBuilding(Building building, int tier) {
		Building.TierInfo info = building.GetTierInfo(tier);

		if (info == null) {
			return false;
		}

		if (!HasResources(building, tier)) {
			return false;
		}

		foreach (var resource in info.Cost) {
			ResourceManager.ins.AddResource(resource.Type, -resource.MaxValue);
		}

		return true;
	}


	public List<Building> GetBuildingsOfType(Resource.ResourceTypes type) {
		return builtBuildings.FindAll((b) => b.Type == type);
	}


	public void SelectBuilding(Building b) {
		BuildingInfo.ins.SetInfo(b);
	}


	public void RemoveBuildingFromList(Building b) {
		builtBuildings.Remove(b);
	}
}
