﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonClickFX : MonoBehaviour {
	public AudioClip Clip;

	public void Click() {
		GameManager.ins.SFXPlayer.PlayOneShot(Clip);
	}
}
