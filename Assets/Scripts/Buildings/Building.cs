﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building : MonoBehaviour, IAdjustable {
	[System.Serializable]
	public class TierInfo {
		public List<Resource> Cost = new List<Resource>();
		public List<Resource> Stats = new List<Resource>();
		public Sprite BuildingSprite;
		public GameObject ExtraGameObject;
		public int Pollution;
	}

	public static event Action OnResourceChanged;

	public Resource.ResourceTypes Type;
	public AudioClip PlaceClip;
	public string ID;
	public string Name;
	public int Tier {
		get;
		private set;
	}
	public string Description;
	[SerializeField]
	private List<TierInfo> Tiers = new List<TierInfo>();

	private Biome biome;
	private Planet planet;
	private SpriteRenderer spriteRenderer;
	private int[] resourceBonuses = new int[] { 1, 2, 3 };
	private float[] pollutionBonuses = new float[] { 0.1f, 0.2f, 0.3f };
	private int resourceBonus;
	private float pollutionBonus;


	private void OnEnable() {
		ResearchManager.OnResearchUpdated += ResearchUpdated;
		GameManager.OnDayChanged += DoDailyAction;
	}

	private void OnDisable() {
		ResearchManager.OnResearchUpdated -= ResearchUpdated;
		GameManager.OnDayChanged -= DoDailyAction;
	}


	private void Awake() {
		spriteRenderer = GetComponent<SpriteRenderer>();
    }


	private void ResearchUpdated() {
		int currentTier = ResearchManager.ins.GetCurrentTier(ResearchManager.ResearchID.BUILDINGS_1);

		if (currentTier > 0 && ResearchManager.ins.HasResearched(ResearchManager.ResearchID.BUILDINGS_1, currentTier)) {
			resourceBonus = resourceBonuses[currentTier - 1];
		}

		currentTier = ResearchManager.ins.GetCurrentTier(ResearchManager.ResearchID.BUILDINGS_2);

		if (currentTier > 0 && ResearchManager.ins.HasResearched(ResearchManager.ResearchID.BUILDINGS_2, currentTier)) {
			pollutionBonus = pollutionBonuses[currentTier - 1];
		}
	}


	private void Start() {
		Tier = 1;
	}


	public TierInfo GetTierInfo(int tier) {
		if (tier > 0 && Tiers.Count >= tier) {
			return Tiers[tier - 1];
		}

		return null;
	}


	public void Upgrade() {
		if (Tiers.Count > Tier) {
			TierInfo oldTier = GetTierInfo(Tier);
			Tier++;
			TierInfo newTier = GetTierInfo(Tier);

			// Set old values to new ones
			foreach (Resource r in newTier.Stats) {
				r.SetValue(oldTier.Stats.Find((s) => s.Type == r.Type).CurrentValue);
			}

			if (spriteRenderer != null) {
				if (newTier != null && newTier.BuildingSprite != null) {
					spriteRenderer.sprite = newTier.BuildingSprite;
				}
			}

			if (newTier.ExtraGameObject != null) {
				newTier.ExtraGameObject.SetActive(true);
            }

			Vector3 size = GetComponent<SpriteRenderer>().sprite.bounds.size;
            GetComponent<BoxCollider2D>().size = size;
			GetComponent<BoxCollider2D>().offset = new Vector3(0, size.y / 2f);
		}
	}


	public Biome GetBiome(Planet p) {
		Vector2 dir = p.transform.position - transform.position;
		Vector2 pos = transform.position;
		pos -= (0.02f * dir.normalized);
		RaycastHit2D[] hits = Physics2D.RaycastAll(pos, dir, 0.3f);

		foreach (var hit in hits) {
			if (hit.collider != null && hit.transform.GetComponent<Biome>()) {
				return hit.transform.GetComponent<Biome>();
			}
		}

		return null;
	}


	public void PlaceBuilding(Planet p) {
		this.planet = p;

		Helper.AlignWithPlanet(p, transform, 0.2f);
		p.AddToContainer(transform);
		biome = GetBiome(p);

		transform.position += new Vector3(0, 0, 0.2f);

		StartCoroutine(MoveToPlanet());
    }


	private IEnumerator MoveToPlanet() {
		float timer = 0;
		Vector3 startPos = transform.position;
		Vector3 newPos = Helper.GetPositionOnPlanet(planet, transform);

		GameManager.ins.SFXPlayer.PlayOneShot(PlaceClip);

		while (timer < 1) {
			timer += Time.deltaTime * 6f;

			transform.position = Vector3.Lerp(startPos, newPos, Mathf.SmoothStep(0, 1, timer));

			yield return null;
		}

		transform.position = newPos;
		
		planet.Shake(transform.position - planet.transform.position, -0.04f);
		ParticleManager.ins.PlayParticle(ParticleManager.ParticleType.PLACING_SMOKE, transform.position + new Vector3(0, 0, -2), Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z + 180));
	}


	public int GetResourcePerDay(Resource.ResourceTypes type) {
		TierInfo info = GetTierInfo(Tier);

		if (info != null) {
			Resource resource = info.Stats.Find((r) => r.Type == type);

			if (resource != null) {
				return resource.MaxValue + resourceBonus;
			}
		}

		return 0;
	}


	public virtual void DoDailyAction() {
		if (biome != null && biome.ResourceType == Type) {
			ResourceManager.ins.AddResource(biome.ResourceType, GetResourcePerDay(Type));
			GameManager.ins.IncreasePollution(Mathf.FloorToInt((float)GetTierInfo(Tier).Pollution * (1f - pollutionBonus)));
        }
	}


	public void Highlight() {
		GetComponent<SpriteRenderer>().material.SetFloat("_FlashAmount", 0.7f);
	}


	public void DontHighlight() {
		GetComponent<SpriteRenderer>().material.SetFloat("_FlashAmount", 0f);
	}


	public void Adjust() {
		Helper.AlignWithPlanet(planet, transform);
	}


	protected void UpdatedResources() {
		if (OnResourceChanged != null) {
			OnResourceChanged();
		}
	}


	public virtual void DestroyBuilding() {
		BuildingManager.ins.RemoveBuildingFromList(this);
		Destroy(gameObject);
	}
}
