﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class House : Building {
	public int Capacity {
		get {
			TierInfo info = GetTierInfo(Tier);
			return info.Stats.Find((s) => s.Type == Resource.ResourceTypes.POPULATION).MaxValue;
		}
	}
	public bool HasRoom {
		get {
			TierInfo info = GetTierInfo(Tier);
			Resource r = info.Stats.Find((s) => s.Type == Resource.ResourceTypes.POPULATION);

			if (r.CurrentValue < r.MaxValue) {
				return true;
			}

			return false;
		}
	}


	public bool MoveIn() {
		TierInfo info = GetTierInfo(Tier);
		Resource r = info.Stats.Find((s) => s.Type == Resource.ResourceTypes.POPULATION);

		if (r.CurrentValue < r.MaxValue) {
			r.AddValue(1);
			UpdatedResources();
            return true;
		}

		return false;
	}


	public override void DoDailyAction() {

	}

	public override void DestroyBuilding() {
		float offset = 0.1f;
		TierInfo info = GetTierInfo(Tier);
		Resource r = info.Stats.Find((s) => s.Type == Resource.ResourceTypes.POPULATION);

		for (int i = 0; i < r.CurrentValue; i++) {
			GameManager.ins.SpawnCitizen(transform.position + new Vector3(Random.Range(-offset, offset), Random.Range(-offset, offset), 0));
        }

		base.DestroyBuilding();
	}
}
