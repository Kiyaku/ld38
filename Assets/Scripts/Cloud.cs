﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cloud : MonoBehaviour, IAdjustable {
	public Transform CloudPrefab;
	public Color ColorPolluted;

	private Planet planet;
	private float speed;


	private void Start() {
		speed = Random.Range(1f, 2f);
	}


	private void OnEnable() {
		GameManager.OnDayChanged += CheckPollution;
	}


	private void OnDisable() {
		GameManager.OnDayChanged -= CheckPollution;
	}


	private void CheckPollution() {
		Color newCol = Color.Lerp(Color.white, ColorPolluted, GameManager.ins.GetPollutionPercent());

		foreach (Transform c in transform) {
			c.GetComponent<SpriteRenderer>().color = newCol;
		}
	}


	public void Generate(Planet p) {
		this.planet = p;
		planet.AddToContainer(transform);

		Helper.AlignWithPlanet(p, transform, 0.3f);

		int count = Random.Range(10, 20);
		float length = (float)count * 0.05f;
		float dist = length / (float)count;
		float offset = 0.1f;

		for (int i = 0; i < count; i++) {
			Vector2 pos = new Vector2(dist * i - (length / 2f) + Random.Range(-offset, offset), 0);
			Transform cloud = Instantiate(CloudPrefab);
			cloud.parent = transform;
			cloud.localPosition = pos;
			cloud.localScale = Vector3.one * (Mathf.Lerp(0.2f, 0.8f, Mathf.Sin(((float)i / (float)count) * 180 * Mathf.Deg2Rad)) + Random.Range(0, 0.2f));
			Helper.AlignWithPlanet(p, cloud, 0.3f);
		}

		transform.localScale = Vector3.one * Random.Range(0.6f, 0.8f);
	}


	private void Update() {
		transform.RotateAround(planet.transform.position, Vector3.back, Time.deltaTime * speed);
	}


	public void Adjust() {
		Helper.AlignWithPlanet(planet, transform, 0.3f);

		foreach (Transform t in transform) {
			Helper.AlignWithPlanet(planet, t, 0.3f);
		}
	}
}
