﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetDebris : MonoBehaviour {
	private Transform cargo;
	private Vector3 offset;
	private float offsetDist = 0.3f;


	private void Awake() {
		GetComponent<SpriteRenderer>().sprite = GameManager.ins.DebrisSprites[Random.Range(0, GameManager.ins.DebrisSprites.Count)];
		transform.localScale = Vector3.one * Random.Range(0.4f, 0.8f);
    }


	public void PickUp(Cargo cargo) {
		bool success = cargo.AddToStorage(transform);

		if (!success)
			return;

		this.cargo = cargo.transform;

		GetComponent<Rigidbody2D>().isKinematic = true;
		GetComponent<Rigidbody2D>().velocity = Vector2.zero;
		GetComponent<Collider2D>().enabled = false;

		offset = new Vector3(Random.Range(-offsetDist, offsetDist), Random.Range(-offsetDist, offsetDist), 0);
    }


	private void Update() {
		if (cargo != null) {
			transform.position = Vector3.Lerp(transform.position, cargo.position + offset, Time.deltaTime * 2f);
		}
	}
}
